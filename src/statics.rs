use std::collections::HashMap;
use std::env;
use std::sync::RwLock;

use lazy_static::lazy_static;
use mysql;
use neweden::Galaxy;
use radix_trie::{Trie, TrieCommon};
use serde_derive::Deserialize;
use serenity::model::id::{ChannelId, GuildId};
use toml;

use crate::database;

pub struct NameLookupTrie<V>(Trie<String, V>);

impl<V> NameLookupTrie<V> {
    pub fn new() -> Self {
        Self(Trie::new())
    }
    pub fn insert(&mut self, key: String, value: V) {
        self.0.insert(key, value);
    }
    #[allow(dead_code)]
    pub fn lookup<'a>(&self, key: &'a str) -> Vec<&V> {
        let values = self
            .0
            .get_raw_descendant(key)
            .map(|node| {
                node.iter()
                    .take(25 + 1)
                    .map(|(_, value)| value)
                    .collect::<Vec<_>>()
            })
            .unwrap_or(vec![]);
        values
    }
    pub fn lookup_key<'a>(&self, key: &'a str) -> Vec<&String> {
        let values = self
            .0
            .get_raw_descendant(key)
            .map(|node| node.iter().take(25 + 1).map(|(k, _)| k).collect::<Vec<_>>())
            .unwrap_or(vec![]);
        values
    }

    #[allow(dead_code)]
    pub fn get<'a>(&self, key: &'a str) -> Option<&V> {
        self.0.get(key)
    }
}

#[derive(Deserialize)]
#[serde(default)]
pub struct Database {
    pub pathfinder: Option<String>,
    pub sde: String,
    pub update_interval: u64,
}

impl Default for Database {
    fn default() -> Self {
        Database {
            pathfinder: None,
            sde: "file:///usr/share/eve-pathfinder-discord/neweden.sqlite".to_owned(),
            update_interval: 100,
        }
    }
}

#[derive(Deserialize)]
pub struct Config {
    pub database: Database,
    pub discord_token: String,
    #[serde(default)]
    pub allowlist: HashMap<String, Allowlist>,
}

impl Config {
    pub fn is_allowed(&self, guild_id: Option<GuildId>, channel_id: ChannelId) -> bool {
        if self.allowlist.len() == 0 {
            return true;
        }

        match (guild_id, channel_id) {
            (Some(sid), cid) => {
                for e in self.allowlist.values() {
                    match e {
                        Allowlist::Channel {
                            guild: sid2,
                            channel: cid2,
                        } => {
                            if sid2 == &sid && cid2 == &cid {
                                return true;
                            }
                        }
                    }
                }
            }
            (None, cid) => {
                for e in self.allowlist.values() {
                    match e {
                        Allowlist::Channel {
                            guild: _,
                            channel: cid2,
                        } => {
                            if cid2 == &cid {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        false
    }

    pub fn from_file<P: AsRef<std::path::Path>>(path: P) -> Self {
        let content = std::fs::read_to_string(path).expect("failed to read config file");
        toml::from_str(&content).expect("error parsing config as toml")
    }
}

#[derive(Deserialize)]
#[serde(tag = "type")]
pub enum Allowlist {
    Channel { guild: GuildId, channel: ChannelId },
}

lazy_static! {
    pub static ref CONFIG: Config = {
        let path = env::var("CONFIG").expect("Expected CONFIG environment variable");
        Config::from_file(path)
    };
    pub static ref EDEN: neweden::Universe = {
        use neweden::source::sqlite::DatabaseBuilder;
        DatabaseBuilder::new(&CONFIG.database.sde)
            .build()
            .expect("cannot connect to sqlite")
    };
    pub static ref UNIVERSE: RwLock<neweden::ExtendedUniverse<'static, neweden::Universe>> = {
        if let Some(uri) = &CONFIG.database.pathfinder {
            let opts = mysql::Opts::from_url(&uri).expect("Expected valid MYSQL URI");
            let pool = mysql::Pool::new(opts).expect("connection to 'pathfinder' failed");
            let mut conn = pool.get_conn().expect("cannot get connection");
            let universe = database::extended_universe(&mut conn, &*EDEN)
                .expect("Cannot load universe from database");
            RwLock::new(universe)
        } else {
            let builder = neweden::builder::ExtendedUniverseBuilder::new(&*EDEN);
            RwLock::new(builder.build())
        }
    };
    pub static ref LOOKUP: RwLock<NameLookupTrie<neweden::SystemId>> = {
        let universe = UNIVERSE.read().unwrap();
        let mut trie = NameLookupTrie::new();
        for system in universe.systems() {
            trie.insert(system.name.clone(), system.id);
        }
        RwLock::new(trie)
    };
}

pub fn update_universe() {
    if let Some(uri) = &CONFIG.database.pathfinder {
        let opts = mysql::Opts::from_url(&uri).expect("Expected valid MYSQL URI");
        let pool = mysql::Pool::new(opts).expect("connection to 'pathfinder' failed");
        let mut conn = pool.get_conn().expect("cannot get connection");
        let mut universe = UNIVERSE.write().unwrap();
        *universe = database::extended_universe(&mut conn, &*EDEN)
            .expect("Cannot load universe from database");
    }
}
