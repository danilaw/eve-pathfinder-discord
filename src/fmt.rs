use anyhow;
use neweden::{navigation::PathElement, ConnectionType};
use std::fmt::Write;

pub(crate) fn dotlan<'a>(
    path: &neweden::navigation::Path<'a>,
    preference: &neweden::navigation::Preference,
) -> anyhow::Result<String> {
    let mut s = String::new();
    let mut last: Option<String> = None;
    let mut follow_path = false;

    write!(s, "https://evemaps.dotlan.net/route/")?;

    match preference {
        neweden::navigation::Preference::Highsec => {
            write!(s, "2:")?;
        }
        neweden::navigation::Preference::LowsecAndNullsec => {
            write!(s, "3:")?;
        }
        _ => {}
    }

    for element in path {
        match element {
            PathElement::Waypoint(system) => {
                write!(s, "{:}", system.name)?;
                follow_path = true;
                last = None;
            }
            PathElement::System(system) if follow_path => {
                write!(s, "{:}", system.name)?;
            }
            PathElement::System(system) => {
                last = Some(system.name.clone());
            }
            PathElement::Connection(ConnectionType::Stargate(_)) if follow_path => {
                write!(s, ":")?;
                follow_path = false;
                last = None;
            }
            PathElement::Connection(ConnectionType::Wormhole(_))
            | PathElement::Connection(ConnectionType::Bridge(_)) => {
                if let Some(sys) = last {
                    write!(s, "{:}", sys)?;
                }
                write!(s, "::")?;
                follow_path = true;
                last = None;
            }
            _ => {}
        }
    }
    Ok(s)
}

pub(crate) fn direct<'a>(path: &neweden::navigation::Path<'a>) -> anyhow::Result<String> {
    let mut s = String::new();

    for element in path {
        match element {
            PathElement::Waypoint(system) => {
                write!(s, "{:}", system.name)?;
            }
            PathElement::System(system) => {
                write!(s, "{:}", system.name)?;
            }
            PathElement::Connection(ConnectionType::Stargate(_)) => {
                write!(s, " -> ")?;
            }
            PathElement::Connection(ConnectionType::Wormhole(_)) => {
                write!(s, " => ")?;
            }
            PathElement::Connection(ConnectionType::Bridge(_)) => {
                write!(s, " >>> ")?;
            }
        }
    }
    Ok(s)
}
