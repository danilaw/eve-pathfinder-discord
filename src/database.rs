use anyhow;
use mysql::prelude::*;
use mysql::*;
use neweden::{self, Galaxy};
use std::collections::HashSet;

#[allow(dead_code)]
pub(crate) fn universe(conn: &mut mysql::PooledConn) -> anyhow::Result<neweden::Universe> {
    let mut builder = neweden::builder::UniverseBuilder::new();

    builder = {
        let query = conn
            .query_iter("SELECT id, name, securityStatus, x, y, z FROM system")
            .expect("expected query to execute");

        for row in query {
            let (id, name, sec, x, y, z): (u32, String, f64, f64, f64, f64) =
                from_row(row.expect("row"));
            let sys = neweden::System {
                id: id.into(),
                name: name,
                coordinate: neweden::Coordinate { x, y, z },
                security: neweden::Security(sec as f32),
            };

            // add system to builder
            builder = builder.system(sys);
        }
        builder
    };

    builder = {
        let query = conn
            .query_iter("SELECT systemId, jumpNodes FROM system_neighbour")
            .expect("expected query to execute");

        for row in query {
            let (from, nodes): (u32, String) = from_row(row.expect("row"));

            let tos = nodes.split(":").map(|id| id.parse::<u32>().unwrap());
            for to in tos {
                let conn = neweden::Connection {
                    from: from.into(),
                    to: to.into(),
                    type_: neweden::ConnectionType::Stargate(neweden::StargateType::Local),
                };

                // add connection to builder
                builder = builder.connection(conn);
            }
        }
        builder
    };
    Ok(builder.build())
}

pub(crate) fn extended_universe<'a>(
    conn: &mut mysql::PooledConn,
    universe: &'a neweden::Universe,
) -> anyhow::Result<neweden::ExtendedUniverse<'a, neweden::Universe>> {
    let lookup = universe
        .connections()
        .into_iter()
        .map(|(a, b)| if a < b { (a, b) } else { (b, a) })
        .collect::<HashSet<(neweden::SystemId, neweden::SystemId)>>();

    let mut builder = neweden::builder::ExtendedUniverseBuilder::new(universe);
    let query = conn
        .query_iter(
            "
            SELECT
                f.systemId as `from`,
                t.systemId as `to`
            FROM
                system f,
                system t,
                connection c
            WHERE
                c.source = f.id
                AND c.target=t.id
        ",
        )
        .expect("expected query to execute");
    for row in query {
        let (from, to): (u32, u32) = from_row(row.expect("row"));
        // don't duplicate entries
        let from_id = from.into();
        let to_id = to.into();
        let check = if from_id < to_id {
            (from_id, to_id)
        } else {
            (to_id, from_id)
        };
        if lookup.contains(&check) {
            continue;
        }
        let connection = neweden::Connection {
            from: from_id,
            to: to_id,
            type_: neweden::ConnectionType::Wormhole(neweden::WormholeType::Unknown),
        };
        builder = builder.connection(connection);

        // we need to reverse the connection, since neweden is handling connections
        let connection = neweden::Connection {
            from: to_id,
            to: from_id,
            type_: neweden::ConnectionType::Wormhole(neweden::WormholeType::Unknown),
        };
        builder = builder.connection(connection);
    }
    Ok(builder.build())
}
