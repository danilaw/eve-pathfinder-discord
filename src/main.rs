mod commands;
mod database;
mod fmt;
mod parse;
mod statics;

use log;
use std::time::Duration;

use neweden::{self, Galaxy};
use serenity::{
    async_trait, model::application::command::Command,
    model::application::interaction::Interaction, model::gateway::Ready, prelude::*,
};

use statics::UNIVERSE;

struct BotHandler;

#[async_trait]
impl EventHandler for BotHandler {
    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        match interaction {
            Interaction::ApplicationCommand(command) => {
                log::info!("Received command interaction: {:#?}", command);

                match command.data.name.as_str() {
                    "route" => commands::route::run(ctx, &command).await,
                    _ => {}
                };
            }
            Interaction::Autocomplete(command) => match command.data.name.as_str() {
                "route" => commands::route::autocomplete(ctx, &command).await,
                _ => {}
            },
            _ => {}
        }
    }

    async fn ready(&self, ctx: Context, _ready: Ready) {
        let cmd = Command::create_global_application_command(&ctx.http, |command| {
            commands::route::register(command)
        })
        .await;

        log::debug!("created commands: {:?}", cmd);
        log::info!("Commands created.");
    }
}

async fn update_from_database() {
    let mut timer = tokio::time::interval(Duration::from_secs(
        statics::CONFIG.database.update_interval,
    ));
    timer.tick().await;
    loop {
        statics::update_universe();
        timer.tick().await;
    }
}

#[tokio::main]
async fn main() {
    // Configure the client with your Discord bot token in the environment.
    println!("loading systems from pathfinder database");
    println!(
        "loaded {:?} systems",
        UNIVERSE.read().unwrap().systems().len()
    );

    tokio::spawn(update_from_database());

    // Create a new instance of the Client, logging in as a bot. This will
    // automatically prepend your bot token with "Bot ", which is a requirement
    // by Discord for bot users.
    let mut client = Client::builder(&statics::CONFIG.discord_token, GatewayIntents::empty())
        .event_handler(BotHandler)
        .await
        .expect("Err creating client");

    // Finally, start a single shard, and start listening to events.
    //
    // Shards will automatically attempt to reconnect, and will perform
    // exponential backoff until it reconnects.
    if let Err(why) = client.start().await {
        println!("Client error: {:?}", why);
    }
}
