use thiserror::Error;

use lazy_static::lazy_static;
use regex;

use crate::statics;

lazy_static! {
    static ref RE_ROUTE: regex::Regex = regex::Regex::new(
        r"^!route (?P<from>[\w\-]+?)[,\s]+(?P<to>[\w\-]+?)(?: via (?P<via>[\w\-,\s]+?))?(?: using (?P<using>[\w\-,\s]+?))?$"
    ).unwrap();
}

#[derive(Error, Debug)]
#[allow(dead_code)]
pub enum ParseRouteError {
    #[error("System **{0}** does not exist")]
    SystemDoesNotExist(String),
    #[error("Cannot parse `?route` command. Usage: ?route FROM,TO [via SYS] [using SYS]")]
    ParsingError,
    #[error("Unknown parsing error")]
    Unknown,
}

pub fn sysid(s: &str) -> Result<neweden::SystemId, ParseRouteError> {
    Ok(statics::LOOKUP
        .read()
        .unwrap()
        .get(s)
        .ok_or(ParseRouteError::SystemDoesNotExist(s.to_owned()))?
        .clone())
}
