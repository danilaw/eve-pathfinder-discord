// (c) 2023 by Danilaw
use crate::fmt::{direct, dotlan};
use crate::parse::ParseRouteError;
use crate::{parse::sysid, statics};
use anyhow;
use neweden::{self, Navigatable};
use serenity::builder::{CreateApplicationCommand, CreateEmbed};
use serenity::model::application::command::CommandOptionType;
use serenity::model::application::interaction::{
    application_command::ApplicationCommandInteraction, InteractionResponseType,
};
use serenity::model::interactions::autocomplete::AutocompleteInteraction;
use serenity::model::prelude::application_command::{CommandDataOption, CommandDataOptionValue};
use serenity::prelude::Context;

/// Register the slash command with all options. This will be called by the main handler
/// during ready().
pub fn register(command: &mut CreateApplicationCommand) -> &mut CreateApplicationCommand {
    command
        .name("route")
        .description("a route through NewEden using w-space")
        .create_option(|option| {
            option
                .name("from")
                .description("Start system")
                .kind(CommandOptionType::String)
                .required(true)
                .set_autocomplete(true)
        })
        .create_option(|option| {
            option
                .name("to")
                .description("End system")
                .kind(CommandOptionType::String)
                .required(true)
                .set_autocomplete(true)
        })
        .create_option(|option| {
            option
                .name("bridge")
                .description("Assume titans in the following systems")
                .kind(CommandOptionType::String)
                .required(false)
                .set_autocomplete(true)
        })
        .create_option(|option| {
            option
                .name("preference")
                .description("Preference: Shortest, Secure, Less Secure")
                .kind(CommandOptionType::String)
                .required(false)
                .add_string_choice("shortest", "Shortest")
                .add_string_choice("secure", "Secure")
                .add_string_choice("less-secure", "Less Secure")
        })
}

struct Options {
    from: String,
    to: String,
    bridge: Option<String>,
    pref: neweden::navigation::Preference,
}

fn str(opt: &Option<CommandDataOptionValue>) -> String {
    if let Some(CommandDataOptionValue::String(s)) = opt {
        s.to_string()
    } else {
        panic!("unreachable")
    }
}

impl Options {
    fn from_command(command: &ApplicationCommandInteraction) -> Self {
        let mut opts = Self {
            from: "Jita".to_owned(),
            to: "Camal".to_owned(),
            bridge: None,
            pref: neweden::navigation::Preference::Shortest,
        };

        for option in &command.data.options {
            match option.name.as_ref() {
                "from" => {
                    opts.from = str(&option.resolved);
                }
                "to" => {
                    opts.to = str(&option.resolved);
                }
                "bridge" => {
                    opts.bridge = Some(str(&option.resolved));
                }
                "preference" => {
                    opts.pref = match &option.resolved {
                        Some(CommandDataOptionValue::String(s)) => match s.as_ref() {
                            "Shortest" => neweden::navigation::Preference::Shortest,
                            "Less Secure" => neweden::navigation::Preference::LowsecAndNullsec,
                            "Secure" => neweden::navigation::Preference::Highsec,
                            _ => neweden::navigation::Preference::Shortest,
                        },
                        _ => neweden::navigation::Preference::Shortest,
                    };
                }
                _ => {}
            }
        }

        opts
    }
}

/// Run the slash command
/// We will check if the current guild and channel is in the allowlist
/// (no allowlist means everyone is allowed to use it).
/// We then parse the options and hand them to route() to generate a Route enum for us.
pub async fn run(ctx: Context, command: &ApplicationCommandInteraction) {
    if statics::CONFIG.is_allowed(command.guild_id, command.channel_id) {
        let opts = Options::from_command(&command);
        match route(opts.from, opts.to, opts.bridge, opts.pref) {
            Ok(route) => {
                command
                    .create_interaction_response(&ctx.http, |response| {
                        response
                            .kind(InteractionResponseType::ChannelMessageWithSource)
                            .interaction_response_data(|m| {
                                m.embed(|e| format(e, route));
                                m
                            })
                    })
                    .await
                    .expect("error creating response");
            }
            Err(e) => {
                command
                    .create_interaction_response(&ctx.http, |response| {
                        response
                            .kind(InteractionResponseType::ChannelMessageWithSource)
                            .interaction_response_data(|m| {
                                m.content(format!("Error {:?}", e));
                                m
                            })
                    })
                    .await
                    .expect("error creating response");
            }
        }
    } else {
        command
            .create_interaction_response(&ctx.http, |response| {
                response
                    .kind(InteractionResponseType::ChannelMessageWithSource)
                    .interaction_response_data(|m| {
                        m.content("You are not allowed to use this command");
                        m
                    })
            })
            .await
            .expect("error creating response");
    }
}

pub async fn autocomplete(ctx: Context, options: &AutocompleteInteraction) {
    let focused = options.data.options.iter().filter(|o| o.focused == true);

    for option in focused {
        let names = {
            let lookup = statics::LOOKUP.read().unwrap();

            let result = match (option.name.as_str(), &option.resolved) {
                // Doctrine option
                ("from", Some(CommandDataOptionValue::String(s))) => lookup.lookup_key(s),
                ("to", Some(CommandDataOptionValue::String(s))) => lookup.lookup_key(s),
                ("bridge", Some(CommandDataOptionValue::String(s))) => lookup.lookup_key(s),
                _ => vec![],
            };
            result.into_iter().cloned().collect::<Vec<_>>()
        };
        options
            .create_autocomplete_response(&ctx.http, |response| {
                if names.len() <= 25 {
                    for name in &names {
                        response.add_string_choice(name, name);
                    }
                }
                response
            })
            .await;
    }
}

/// Format a route enum as an embed
fn format(e: &mut CreateEmbed, r: Route) -> &mut CreateEmbed {
    match r {
        Route::WithPath {
            from,
            to,
            dotlan,
            path,
            jumps,
            elapsed,
        } => {
            e.title("Pathfinder Bot");
            e.description(format!("Find shortest route."));
            e.fields(vec![
                ("From", format!("{:}", from), true),
                ("To", format!("{:}", to), true),
                ("Jumps", format!("{:}", jumps), true),
            ]);
            e.field("Dotlan", dotlan, false);
            e.field("Route", path, false);
            e.footer(|f| {
                f.text(format!(
                    "Query time: {:}ms. bot by Danilaw",
                    (elapsed.as_micros() as f64 / 1_000.0f64)
                ));
                f
            });
        }
        Route::WithoutPath { from, to, elapsed } => {
            e.title("Pathfinder Bot");
            e.description(format!("Find shortest route"));
            e.fields(vec![
                ("From", format!("{:}", from), true),
                ("To", format!("{:}", to), true),
            ]);
            e.field("Error", "No route found", false);
            e.footer(|f| {
                f.text(format!(
                    "Query time: {:}ms. bot by Danilaw",
                    (elapsed.as_micros() as f64 / 1_000.0f64)
                ));
                f
            });
        }
    }
    e
}

/// Intermediate representation of a route data, which will be formatted
enum Route {
    WithPath {
        from: String,
        to: String,
        dotlan: String,
        path: String,
        jumps: usize,
        elapsed: std::time::Duration,
    },
    WithoutPath {
        from: String,
        to: String,
        elapsed: std::time::Duration,
    },
}

/// Create a route
fn route(
    from_s: String,
    to_s: String,
    bridge_s: Option<String>,
    pref: neweden::navigation::Preference,
) -> Result<Route, anyhow::Error> {
    let instant = std::time::Instant::now();

    let eden = statics::UNIVERSE.read().unwrap();
    let universe = {
        let mut builder = neweden::builder::ExtendedUniverseBuilder::new(&*eden);
        if let Some(sys) = bridge_s {
            builder = builder.bridge(
                sysid(&sys)?,
                neweden::BridgeType::Titan(neweden::JumpdriveSkills::new(5, 5).into()),
            );
        }
        builder.build()
    };

    let from = universe
        .get_system(&sysid(&from_s)?)
        .ok_or(ParseRouteError::SystemDoesNotExist(from_s.clone()))?;

    let to = universe
        .get_system(&sysid(&to_s)?)
        .ok_or(ParseRouteError::SystemDoesNotExist(to_s.clone()))?;

    let path = {
        let mut builder = neweden::navigation::PathBuilder::new(&universe);
        builder = builder.waypoint(from);
        // for via in intent.via {
        //     let waypoint = universe.get_system(&via).unwrap();
        //     builder = builder.waypoint(waypoint);
        // }
        // final destination
        builder = builder.waypoint(to);
        builder = builder.prefer(pref.clone());
        builder.build()
    };

    let elapsed = std::time::Instant::now() - instant;
    if let Some(path) = path {
        Ok(Route::WithPath {
            from: from_s,
            to: to_s,
            dotlan: dotlan(&path, &pref)?,
            path: direct(&path)?,
            jumps: path.jumps(),
            elapsed,
        })
    } else {
        Ok(Route::WithoutPath {
            from: from_s,
            to: to_s,
            elapsed,
        })
    }
}
