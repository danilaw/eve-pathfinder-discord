# eve-pathfinder-discord
Discord integration for Pathfinder. The discord application will help you
find paths through your mapped wormholes.

![example](docs/screenshot/screen01.png)

## Prerequisites
You need a discord token allowing for commands and is registered on your server.

## Running
The best way to run cynoup-bot is using kubernetes, since that's what the author does,
so it's well tested.


### Running locally
Copy `config.toml.example` to `config.toml` and fill in the config.
Provide the `CONFIG` environemnt variable during startup pointing towrads your
config.toml. For example:
```sh
$ CONFIG=/path/to/config.toml cargo run --release
```

### Running in kubernetes
Build using docker and upload it to your registry:

```sh
$ docker build .
$ docker tag ....
```

Create your deployment file and map a config.toml into

```yaml
...

      - env:
   	 - name: CONFIG
           value: "/usr/share/cynoup-bot/config.toml"
      - volumes:
        - name: cynoup-config-toml
          configMap:
            name: cynoup-cfg
      - volumeMounts:
        - name: cynoup-config.toml
          mountPath: "/usr/share/cynoup-bot/config.toml"
          subPath: config.toml
```

## Questions

For questions, please contact danilaw in game or find me on the AllianceAuth discord.
