FROM rust:1 as builder
WORKDIR /usr/src/eve-pathfinder-discord
COPY . .
RUN cargo install --path .
RUN curl --output neweden.sqlite.bz2 https://www.fuzzwork.co.uk/dump/sqlite-latest.sqlite.bz2
RUN bunzip2 neweden.sqlite.bz2


FROM debian:bullseye-slim
RUN apt-get update && apt-get install -y libsqlite3-0 && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/eve-pathfinder-discord /usr/local/bin/eve-pathfinder-discord
COPY --from=builder /usr/src/eve-pathfinder-discord/neweden.sqlite /usr/share/eve-pathfinder-discord/neweden.sqlite
CMD ["/usr/local/bin/eve-pathfinder-discord"]
